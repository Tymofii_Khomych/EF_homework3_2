﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL_V2.Entity
{
    public class Word
    {
        [NotMapped]
        public List<KeyParams> ProductLink { get; set; } = new List<KeyParams>();
        public Guid Id { get; set; }
        public string Header { get; set; }
        public string Keyword { get; set; }
    }
}
