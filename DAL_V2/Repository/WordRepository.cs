﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_V2.Repository
{
    public class WordRepository : IWordRepository
    {
        public async Task<bool> Create(Word entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    bool exists = await db.Words.AnyAsync(c => c.Id == entity.Id);
                    if (!exists)
                    {
                        db.Words.Add(entity);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<bool> Delete(Word entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var wordToDelete = await db.Words.FindAsync(entity.Id);

                    if (wordToDelete != null)
                    {
                        db.Words.Remove(wordToDelete);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<Word> GetById(Guid id)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var word = await db.Words.FindAsync(id);

                    if (word != null)
                    {
                        return word;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<Word>> Select()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var word = await db.Words
                                       .Include(c => c.ProductLink)
                                       .ToListAsync();
                    if (word.Any())
                    {
                        return word;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<Word>> SelectIncludeKeyParamsProducts()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var word = await db.Words
                                       .Include(c => c.ProductLink)
                                       .ToListAsync();

                    if (word.Any())
                    {
                        return word;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<Word> Update(Word entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var existingWord = await db.Words.FindAsync(entity.Id);

                    if (existingWord != null)
                    {
                        existingWord.Header = entity.Header;
                        existingWord.Keyword = entity.Keyword;
                        existingWord.ProductLink = entity.ProductLink.ToList();
                        await db.SaveChangesAsync();

                        return existingWord;
                    }
                    else
                    {
                        return null;
                    }

                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}
