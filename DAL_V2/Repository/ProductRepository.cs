﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_V2.Repository
{
    public class ProductRepository : IProductRepository
    {
        public async Task<bool> Create(Product entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    bool exists = await db.Product.AnyAsync(k => k.Id != entity.Id);
                    if (!exists)
                    {
                        db.Product.Add(entity);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<bool> Delete(Product entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var product = await db.Product.FindAsync(entity.Id);

                    if (product != null)
                    {
                        db.Product.Remove(product);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<Product> GetById(Guid id)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var product = await db.Product.FindAsync(id);

                    if (product != null)
                    {
                        return product;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<Product> GetByIdIncludWord(string name)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var product = await db.Product.FindAsync(name);

                    if (product != null)
                    {
                        return product;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<Product>> Select()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var products = await db.Product
                                       .Include(k => k.KeyWords)
                                       .Include(k => k.Category)
                                       .Include(k => k.Carts)
                                       .ToListAsync();
                    if (products.Any())
                    {
                        return products;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<Product>> SelectIncludeCategory()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var product = await db.Product
                                       .Include(c => c.Category)
                                       .ToListAsync();

                    if (product.Any())
                    {
                        return product;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<Product> Update(Product entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var product = await db.Product.FindAsync(entity.Id);

                    if (product != null)
                    {
                        product.KeyWords = entity.KeyWords.ToList();
                        product.Name = entity.Name;
                        product.Price = entity.Price;
                        product.ActionPrice = entity.ActionPrice;
                        product.DescriptionField1 = entity.DescriptionField1;
                        product.DescriptionField2 = entity.DescriptionField2;
                        product.ImageUrl = entity.ImageUrl;
                        product.Category = entity.Category;
                        product.Carts = entity.Carts.ToList();
                        await db.SaveChangesAsync();

                        return product;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}
