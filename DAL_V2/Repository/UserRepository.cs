﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_V2.Repository
{
    public class UserRepository : IUserRepository
    {
        public async Task<bool> Create(User entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    bool exists = await db.User.AnyAsync(c => c.Id == entity.Id);
                    if (!exists)
                    {
                        db.User.Add(entity);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<bool> Delete(User entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var useroDelete = await db.User.FindAsync(entity.Id);

                    if (useroDelete != null)
                    {
                        db.User.Remove(useroDelete);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<User> GetById(Guid id)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var user = await db.User.FindAsync(id);

                    if (user != null)
                    {
                        return user;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<User>> Select()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var user = await db.User
                                       .Include(c => c.Cart)
                                       .ToListAsync();
                    if (user.Any())
                    {
                        return user;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<User> Update(User entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var existingUser = await db.User.FindAsync(entity.Id);

                    if (existingUser != null)
                    {
                        existingUser.Name = entity.Name;
                        existingUser.Login = entity.Login;
                        existingUser.Password = entity.Password;
                        existingUser.Email = entity.Email;
                        existingUser.Cart = entity.Cart.ToList();
                        await db.SaveChangesAsync();

                        return existingUser;
                    }

                    return null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}
