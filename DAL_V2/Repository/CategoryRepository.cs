﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL_V2.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        public async Task<bool> Create(Category entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    bool exists = await db.Category.AnyAsync(c => c.Id == entity.Id);
                    if (!exists)
                    {
                        db.Category.Add(entity);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<bool> Delete(Category entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var categoryToDelete = await db.Category.FindAsync(entity.Id);

                    if (categoryToDelete != null)
                    {
                        db.Category.Remove(categoryToDelete);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<Category> GetById(Guid id)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var category = await db.Category.FindAsync(id);

                    if (category != null)
                    {
                        return category;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<Category>> Select()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var category = await db.Category
                                       .Include(c => c.Products)
                                       .ToListAsync();
                    if (category.Any())
                    {
                        return category;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<Category>> SelectIncludeProducts()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var category = await db.Category
                                       .Include(c => c.Products)
                                       .ToListAsync();

                    if (category.Any())
                    {
                        return category;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<Category> Update(Category entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var existingCategory = await db.Category.FindAsync(entity.Id);

                    if (existingCategory != null)
                    {
                        existingCategory.Name = entity.Name;
                        existingCategory.Icon = entity.Icon;
                        existingCategory.Products = entity.Products.ToList();
                        await db.SaveChangesAsync();

                        return existingCategory;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}
