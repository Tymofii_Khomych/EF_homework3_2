﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL_V2.Repository
{
    public class CartRepository : ICartRepository
    {
        public async Task<bool> Create(Cart entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    bool exists = await db.Cart.AnyAsync(c => c.Id == entity.Id);
                    if (!exists)
                    {
                        db.Cart.Add(entity);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<bool> Delete(Cart entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var cartToDelete = await db.Cart.FindAsync(entity.Id);

                    if (cartToDelete != null)
                    {
                        db.Cart.Remove(cartToDelete);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<Cart> GetById(Guid id)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var cart = await db.Cart
                                      .Include(c => c.User)
                                      .Include(c => c.Product)
                                      .FirstOrDefaultAsync(c => c.Id == id);

                    if (cart != null)
                    {
                        return cart;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<Cart>> Select()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var carts = await db.Cart
                                       .Include(c => c.User)
                                       .Include(c => c.Product)
                                       .ToListAsync();
                    if (carts.Any())
                    {
                        return carts;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<Cart> Update(Cart entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var existingCart = await db.Cart.FindAsync(entity.Id);

                    if (existingCart != null)
                    {
                        existingCart.User = entity.User;
                        existingCart.Product = entity.Product;

                        await db.SaveChangesAsync();

                        return existingCart;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}
