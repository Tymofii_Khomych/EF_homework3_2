﻿using DAL_V2.Entity;
using DAL_V2.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL_V2.Repository
{
    public class KeyParamsRepository : IKeyParamsRepository
    {
        public async Task<bool> Create(KeyParams entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    bool exists = await db.KeyLink.AnyAsync(c => c.Id == entity.Id);
                    if (!exists)
                    {
                        db.KeyLink.Add(entity);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<bool> Delete(KeyParams entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var keyparams = await db.KeyLink.FindAsync(entity.Id);

                    if (keyparams != null)
                    {
                        db.KeyLink.Remove(keyparams);
                        await db.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public async Task<KeyParams> GetById(Guid id)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var keyparams = await db.KeyLink.FindAsync(id);

                    if (keyparams != null)
                    {
                        return keyparams;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<KeyParams>> Select()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var keyparams = await db.KeyLink
                                       .Include(k => k.Product)
                                       .Include(k => k.KeyWord)
                                       .ToListAsync();
                    if (keyparams.Any())
                    {
                        return keyparams;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<IEnumerable<KeyParams>> SelectIncludeWords()
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var keyparams = await db.KeyLink
                                       .Include(k => k.KeyWord)
                                       .ToListAsync();

                    if (keyparams.Any())
                    {
                        return keyparams;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<KeyParams> Update(KeyParams entity)
        {
            using (EntityDatabase db = new EntityDatabase())
            {
                try
                {
                    var existingKeyParams = await db.KeyLink.FindAsync(entity.Id);

                    if (existingKeyParams != null)
                    {
                        existingKeyParams.Product = entity.Product;
                        existingKeyParams.KeyWord = entity.KeyWord;
                        await db.SaveChangesAsync();

                        return existingKeyParams;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}
